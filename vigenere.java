import java.util.Scanner;
import java.io.*;
public class vigenere{
    public static char[][] create_square(String key){
        char[][] square = new char[key.length()][26];
        for(int j=0 ; j<key.length() ; j++){
            for(int i=0;i<26;i++){
                if((int)key.charAt(j) <90 && (int)key.charAt(j) +i > 65){
                    if((int)key.charAt(j) +i > 90){
                        square[j][i] = (char)( ((int)key.charAt(j) + i)-26 );
                    }else{
                        square[j][i] = (char)((int)key.charAt(j) + i);
                    }
                }else if((int)key.charAt(j) <122 && (int)key.charAt(j) +i > 97){
                    if((int)key.charAt(j) +i > 122){
                        square[j][i] = (char)( ((int)key.charAt(j) + i)-26 );
                    }else{
                        square[j][i] = (char)((int)key.charAt(j) + i);
                    }
                }else{
                    square[j][i] = (char)((int)key.charAt(j) + i);
                }
            }
        }
        return square;
    }

    public static String getWord(char[][] square,String word){
        int count =0,let = 0,x=1;
        String fin="";
        for(int i=0; i<word.length();i++){
            if(i>square.length){
                while( i - (square.length * x) >=square.length){
                    x++;
                }
            }
            let = (int)word.charAt(i);
            if( let- 97 >= 0){
                if(i>=square.length){
                    fin+= square[i-square.length * x][let- 97];
                }else{
                    fin+= square[i][let- 97];
                }
            }else if(let- 65 >= 0 && let- 65 <= 25){
                if(i>=square.length){
                    fin+= square[i-square.length * x][let- 65];
                }else{
                    fin+= square[i][let- 65];
                }
            }else{
                fin+=word.charAt(i);
            }
        }
        return fin;
    }
    public static String decypher(char[][] square,String word,String key){
        String de ="";
        int j=0,l=0,re=0,mo=0;
        boolean ke = false,mol=false;
        for(int i=0;i<word.length();i++){
            j=i;
            l=0;
            if(j >= key.length() && i>0){
                if(i%key.length() == 0){
                    mol = true;
                }
                if(mol){
                    mo++;
                }
                System.out.println("yes");
                j = j - (key.length()+mo);
            }
            System.out.println(j + " " + key.length() );
            if(word.charAt(i)>=97 && key.charAt(j)<=122){
                ke =true;
            }else{
                ke = false;
            }
            if(ke){
                re = 97;
            }else{
                re = 65;
            }
            if( (square[j][0]>=65 && square[j][0]<=90) ){
                l = (word.charAt(i) - re)-(key.charAt(j) - 65);
                if(l<=0){
                    l+=26;
                }
                de+=(char)(((int)'A')+l);
            }else if(square[j][0]>=97 && square[j][0]<=122){
                l = (word.charAt(i) - re) - (key.charAt(j) - 97);
                if(l<=0){
                    l+=26;
                }
                de+=(char)(((int)'a')+l);
            }
            /*else{
                l = word.charAt(i)-key.charAt(j);
                de+= square[i][l];
            }*/
            /*if(word.charAt(i)>=65 && word.charAt(i)<=90){
                if(((word.charAt(i)-key.charAt(j))) < 0){
                    de+=(char)((91+ word.charAt(i)-key.charAt(j)));
                }else{
                    de+=(char)((65+ word.charAt(i)-key.charAt(j)));
                }
            }else if(word.charAt(i)>=97 && word.charAt(i)<=122){
                if(((97+ word.charAt(i)-key.charAt(j))) < 97){
                    de+=(char)((123+ word.charAt(i)-key.charAt(j)));
                }else{
                    de+=(char)((97+ word.charAt(i)-key.charAt(j)));
                }
            }else{
                de+=(char)(word.charAt(i)-key.charAt(j));
            }
        }*/
        }
        return de;
    }

    public static void printStart(){
        System.out.println("██╗   ██╗██╗███████╗ ██████╗  "   );
        System.out.println("██║   ██║██║██╔════╝██╔═══██╗  "  );
        System.out.println("██║   ██║██║███████╗██║   ██║  "  );
        System.out.println("╚██╗ ██╔╝██║╚════██║██║▄▄ ██║ "  );
        System.out.println(" ╚████╔╝ ██║███████║╚██████╔╝ " );
        System.out.println("  ╚═══╝  ╚═╝╚══════╝ ╚══▀▀═╝  ");
        System.out.println("   Wealcome to vigenere's square");
        System.out.println("         By Black Shadow");
        System.out.println("             www.gitlab.com/Black1Shadow");
        System.out.println("");
        System.out.println("1- create square");
        System.out.println("2- decypher a string");
        System.out.println("3- cypher a file");
        System.out.println("4- decypher a file");
        System.out.println("5- quit");
        System.out.println("enter option: ");
    }

    public static void main(String[]args){
        /*try{
            File file = new File("./in.txt");
            FileInputStream fis=new FileInputStream(file);
            int r=0;
            while((r=fis.read())!=-1){
                System.out.print((char)r);
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }*/
        printStart();
        Scanner in = new Scanner(System.in);
        Scanner ino = new Scanner(System.in);
        int x =0,xo=0;
        String fin="";
        x = in.nextInt();
        if(x==1){
            System.out.println("enter a key: ");
            String key="";
            key = ino.nextLine();
            System.out.flush();
            System.out.println("[*]Creating square");
            char[][] square = create_square(key);
            System.out.println("[*]Done!");
            System.out.println("1- cypher a string");
            System.out.println("2- quit");
            xo=2;
            xo = in.nextInt();
            if(xo==1){
                System.out.println("enter a String: ");
                String word="";
                word = ino.nextLine();
                System.out.println("[*]Creating word");
                fin = getWord(square,word);
                System.out.println("[*]Done!");
            }
            System.out.flush();
            printStart();
            if(!fin.equals("") && fin!=null){
                System.out.flush();
                System.out.println("your string is: ");
                System.out.println(fin);
            }
        }else if(x==2){
            System.out.flush();
            System.out.println("Enter a Key:");
            String key="";
            key = ino.nextLine();
            System.out.println("Enter a String to decypher:");
            String cy="";
            cy = ino.nextLine();
            char[][] square = create_square(key);
            if(square!=null){
                System.out.print(decypher(square,cy,key));
            }else{
                printStart();
                System.out.println("Bad input ):");
            }
        }
    }
}
